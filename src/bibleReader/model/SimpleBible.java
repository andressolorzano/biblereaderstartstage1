package bibleReader.model;

import java.util.ArrayList;

/**
 * A class that stores a version of the Bible.
 * 
 * @author Chuck Cusack (Provided the interface)
 * @author Dre Solorzano (provided the implementation)
 */
public class SimpleBible {
	// list of all the verses in the bible
	ArrayList<Verse> verses;
	// version of the bible
	String version;
	// title of the bible
	String title;

	/**
	 * Create a new Bible with the given verses and sets the version and title.
	 * 
	 * @param verses A VerseList of the verses of this version of the Bible whose
	 *               version is set to the version of the Bible they came from and
	 *               whose description is set to the title of the Bible.
	 */
	public SimpleBible(VerseList verses) {
		this.verses = new ArrayList<Verse>(verses.copyVerses());
		version = verses.getVersion();
		title = verses.getDescription();
	}

	/**
	 * Returns the number of verses that this Bible is currently storing.
	 * 
	 * @return the number of verses in the Bible.
	 */
	public int getNumberOfVerses() {
		return verses.size();
	}

	/**
	 * Returns which version this object is storing (e.g. ESV, KJV)
	 * 
	 * @return A string representation of the version.
	 */
	public String getVersion() {
		return version;
	}

	/**
	 * Returns the title of this version of the Bible.
	 * 
	 * @return A string representation of the title.
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param ref the reference to look up
	 * @return true if and only if ref is actually in this Bible
	 */
	public boolean isValid(Reference ref) {
		// for loop to search through all verses in the array list to check if
		// the reference is equal to any of the verses' references
		for (Verse verse : verses) {
			if (verse.getReference().equals(ref)) {
				return true;
			}
		}
		return false;

	}

	/**
	 * Return the text of the verse with the given reference
	 * 
	 * @param ref the Reference to the desired verse.
	 * @return A string representation of the text of the verse or null if the
	 *         Reference is invalid.
	 */
	public String getVerseText(Reference ref) {
		// for loop to find the verse based on the reference
		// if found, returns the text of the verse. If not, returns null
		for (Verse verse : verses) {
			if (verse.getReference().equals(ref)) {
				return verse.getText();
			}
		}
		return null;
	}

	/**
	 * Return a Verse object for the corresponding Reference.
	 * 
	 * @param ref A reference to the desired verse.
	 * @return A Verse object which has Reference r, or null if the Reference is
	 *         invalid.
	 */
	public Verse getVerse(Reference ref) {
		// for loop to find the verse based on the reference
		// if found, returns verse. If not, returns null
		for (Verse verse : verses) {
			if (verse.getReference().equals(ref)) {
				return verse;
			}
		}
		return null;
	}

	/**
	 * @param book    the book of the Bible
	 * @param chapter the chapter of the book
	 * @param verse   the verse within the chapter
	 * @return the verse object with reference "book chapter:verse", or null if the
	 *         reference is invalid.
	 */
	public Verse getVerse(BookOfBible book, int chapter, int verse) {
		// Creates a reference from the parameters, then uses the getVerse method
		Reference ref = new Reference(book, chapter, verse);
		return getVerse(ref);
	}
}
