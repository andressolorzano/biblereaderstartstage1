package bibleReader.model;

/**
 * A class which stores a verse.
 * 
 * @author Charles Cusack (Skeleton Code), 2008, edited 2012.
 * @author Dre Solorzano (provided the implementation)
 */
public class Verse implements Comparable<Verse> {

	// text of the verse
	private String text;
	// reference to the verse
	private Reference reference;

	/**
	 * Construct a verse given the reference and the text.
	 * 
	 * @param r The reference for the verse
	 * @param t The text of the verse
	 */
	public Verse(Reference r, String t) {
		text = t;
		reference = r;
	}

	/**
	 * Construct a verse given the book, chapter, verse, and text.
	 * 
	 * @param book    The book of the Bible
	 * @param chapter The chapter number
	 * @param verse   The verse number
	 * @param text    The text of the verse
	 */
	public Verse(BookOfBible book, int chapter, int verse, String text) {
		this.text = text;
		reference = new Reference(book, chapter, verse);
	}

	/**
	 * Returns the Reference object for this Verse.
	 * 
	 * @return A reference to the Reference for this Verse.
	 */
	public Reference getReference() {
		return reference;
	}

	/**
	 * Returns the text of this Verse.
	 * 
	 * @return A String representation of the text of the verse.
	 */
	public String getText() {
		return text;
	}

	/**
	 * Returns a String representation of this Verse, which is a String
	 * representation of the Reference followed by the String representation of the
	 * text of the verse.
	 */
	@Override
	public String toString() {
		return reference.toString() + " " + text;
	}

	/**
	 * Should return true if and only if they have the same reference and text.
	 */
	@Override
	public boolean equals(Object other) {
		// checks if the object is a Verse
		if (other instanceof Verse) {
			Verse otherV = (Verse) other;
			// checks if the other verse has the same as this verse
			if (sameReference(otherV)) {
				// checks if the text of the other verse is the same as this verse
				if (otherV.getText().equals(getText())) {
					return true;
				}
			}
		}
		return false;
	}

	@Override
	public int hashCode() {
		// same concept as the freebie in the Reference class
		return toString().hashCode();
	}

	/**
	 * From the Comparable interface. See the API for how this is supposed to work.
	 */
	@Override
	public int compareTo(Verse other) {
		// compares to see if the references are the same
		if (reference.compareTo(other.getReference()) == 0) {
			// if the text is lexicographically before the other text
			if (text.compareTo(other.getText()) < 0) {
				return -1;
			}
			// if the text is lexicographically after the other text
			else if (text.compareTo(other.getText()) > 0) {
				return 1;
			}
			// if the text is the same
			else {
				return 0;
			}
		}
		// if the references are not the same, then compare the two references
		// to see which comes first
		return reference.compareTo(other.getReference());
	}

	/**
	 * Return true if and only if this verse the other verse have the same
	 * reference. (So the text is ignored).
	 * 
	 * @param other the other Verse.
	 * @return true if and only if this verse and the other have the same reference.
	 */
	public boolean sameReference(Verse other) {
		return getReference().equals(other.getReference());
	}

}
