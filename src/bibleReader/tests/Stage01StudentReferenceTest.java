package bibleReader.tests;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import bibleReader.model.BookOfBible;
import bibleReader.model.Reference;

/*
 * Tests for the Reference class.
 * @author ?
 */

// Make sure you test at least the following:
// --All of the constructors and all of the methods.
// --The equals for success and for failure, especially when they match in 2 of the 3 places
//    (e.g. same book and chapter but different verse, same chapter and verse but different book, etc.)
// --That compareTo is ordered properly for similar cases as the equals tests.
//    (e.g. should Genesis 1:1 compareTo Revelation 22:21 return a positive or negative number?)
// --Any other potentially tricky things.

public class Stage01StudentReferenceTest {

	// A few sample references to get you started.
	private Reference	ruth1_2;
	private Reference	gen3_23;
	private Reference	rev11_4;
	/*
	 * Anything that should be done before each test.
	 * For instance, you might create some objects here that are used by several of the tests.
	 */
	@Before
	public void setUp() throws Exception {
		// A few sample references to get you started.
		ruth1_2 = new Reference(BookOfBible.Ruth, 1, 2);
		gen3_23 = new Reference(BookOfBible.Genesis, 3, 23);
		rev11_4 = new Reference(BookOfBible.Revelation, 11,4);
		
		// TODO Create more objects that the tests will use here.
		// You need to make them fields so they can be seen in the test methods.
	}

	/*
	 * Anything that should be done at the end of each test.  
	 * Most likely you won't need to do anything here.
	 */
	@After
	public void tearDown() throws Exception {
		// You probably won't need anything here.
	}

	/*
	 * Add as many test methods as you think are necessary. Two suggestions (without implementation) are given below.
	 */
	@Test
	public void testEquals() {
		fail("Not yet implemented");
	}

	@Test
	public void testcompareTo() {
		fail("Not yet implemented");
	}

}
