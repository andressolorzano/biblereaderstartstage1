package bibleReader.model;

/**
 * A simple class that stores the book, chapter number, and verse number.
 * 
 * @author Charles Cusack (Skeleton Code), 2008, edited 2012.
 * @author Dre Solorzano (provided the implementation)
 */
public class Reference implements Comparable<Reference> {

	// the book of the reference
	private BookOfBible book;
	// the chapter number of the reference
	private int chapter;
	// the verse number of the reference
	private int verse;

	/**
	 * Construct a reference given the book, chapter number, and verse number
	 * 
	 * @param book    The book for the reference
	 * @param chapter The chapter number for the reference
	 * @param verse   The verse number for the reference
	 */
	public Reference(BookOfBible book, int chapter, int verse) {
		this.book = book;
		this.chapter = chapter;
		this.verse = verse;
	}

	/**
	 * Returns a string representation of the book for this Reference
	 * 
	 * @return A string representation of the book for this Reference
	 */
	public String getBook() {
		return book.toString();
	}

	/**
	 * Returns the BookOfBible object for this Reference
	 * 
	 * @return A reference to the book of this Reference
	 */
	public BookOfBible getBookOfBible() {
		return book;
	}

	/**
	 * Returns the chapter number for this Reference
	 * 
	 * @return Chapter number for this Reference
	 */
	public int getChapter() {
		return chapter;
	}

	/**
	 * Returns the verse number for this Reference
	 * 
	 * @return Verse number for this Reference
	 */
	public int getVerse() {
		return verse;
	}

	/**
	 * This method should return the reference in the form: "book chapter:verse" For
	 * instance, "Genesis 2:3".
	 */
	@Override
	public String toString() {
		return book.toString() + " " + chapter + ":" + verse;
	}

	/**
	 * Should return true if and only if they have the same book, chapter number,
	 * and verse
	 */
	@Override
	public boolean equals(Object other) {
		// checks if the object is a Reference
		if (other instanceof Reference) {
			Reference otherR = (Reference) other;
			// checks if the books are the same
			if (otherR.getBook().equals(getBook())) {
				// checks if the chapter numbers are the same
				if (otherR.getChapter() == getChapter()) {
					// checks if the verse numbers are the same
					if (otherR.getVerse() == getVerse()) {
						return true;
					}
				}
			}
		}
		// if it fails any checks, returns false
		return false;
	}

	@Override
	public int hashCode() {
		// I'll give you this one as a freebie. This is a trick I learned from
		// Dr. McFall.
		// If two instances are the same, their toString method will return the
		// same thing (assuming you implement toString properly).
		// Thus calling hashCode on them will return the same int, so we are
		// honoring the contract that if a.equals(b) then
		// a.hashCode==b.hashCode.
		// Why go to extra trouble to implement hashCode based on the fields and
		// some weird formula when String already has an implementation?
		// (Well, maybe because it is perhaps less efficient, but we'll do it
		// anyway since it probably isn't that bad.)
		return toString().hashCode();
	}

	/**
	 * Compares the two references to check their order
	 * 
	 * @return negative, zero, and positive if this Reference is before, the same,
	 *         or after the other Reference respectively.
	 */
	@Override
	public int compareTo(Reference otherRef) {
		// If the BookOfBible is the same
		if (otherRef.getBookOfBible().compareTo(this.getBookOfBible()) == 0) {
			// If this Reference's chapter comes before the other Reference's
			if (getChapter() < otherRef.getChapter()) {
				return -1;
			}
			// If this Reference's chapter comes after the other Reference's
			else if (getChapter() > otherRef.getChapter()) {
				return 1;
			}
			// If this Reference's chapter is the same as the other Reference's
			else if (getChapter() == otherRef.getChapter()) {
				// If this Reference's verse is before the other Reference's
				if (getVerse() < otherRef.getVerse()) {
					return -1;
				}
				// If this Reference's verse is after the other Reference's
				else if (getVerse() > otherRef.getVerse()) {
					return 1;
				}
				// If this Reference's verse is the same as the other Reference's
				else if (getVerse() == otherRef.getVerse()) {
					return 0;
				}
			}
		}
		// If this Reference's Book comes before the other Reference's
		else if (this.getBookOfBible().compareTo(otherRef.getBookOfBible()) > 0) {
			return 1;
		}
		// If this Reference's Book comes after the other Reference's
		else {
			return -1;
		}
		// Return 0 if all checks fail
		return 0;
	}
}
